import React, {SyntheticEvent} from 'react';
import './App.css';

function App() {

    const data = [
        {
            "htmlFileName": "loerke-bruckner.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:07Z",
            "title": "Anton Bruckner",
            "titleFileAs": "Anton Bruckner",
            "author": "Oskar Loerke",
            "pubId": "urn:uuid:08d1818d-78c1-48a9-863c-cc0544943efa",
            "year": [
                "1976"
            ],
            "publisher": "Suhrkamp Verlag",
            "firstPublished": "1938",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "loerke-atemerde.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:06Z",
            "title": "Atem der Erde",
            "titleFileAs": "Atem der Erde",
            "author": "Oskar Loerke",
            "pubId": "urn:uuid:cb067912-59cd-485f-aa3b-c476131a1a1d",
            "year": [
                "1930"
            ],
            "publisher": "S. Fischer Verlag",
            "firstPublished": "1930",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "loerke-goethe.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:05Z",
            "title": "Der Goethe des westöstlichen Divans",
            "titleFileAs": "Goethe des westöstlichen Divans, Der",
            "author": "Oskar Loerke",
            "pubId": "urn:uuid:1bc8079a-51d1-4264-82f8-3530ed7c8028",
            "year": [
                "1929"
            ],
            "publisher": "S. Fischer Verlag Berlin",
            "firstPublished": "1929",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "loerke-unsicht.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:04Z",
            "title": "Das unsichtbare Reich",
            "titleFileAs": "unsichtbare Reich, Das",
            "author": "Oskar Loerke",
            "pubId": "urn:uuid:0a883da0-ecea-422b-94a5-4d7c4aeb9dd5",
            "year": [
                "1935"
            ],
            "publisher": "S. Fischer Verlag",
            "firstPublished": "1935",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "lisleada-tothaupt.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:03Z",
            "title": "Das tote Haupt",
            "titleFileAs": "tote Haupt, Das",
            "author": "Villiers de L'Isle Adam",
            "pubId": "urn:uuid:8e553cf1-5e8d-48a8-9d8d-038a957cc5cc",
            "year": [
                "1947"
            ],
            "publisher": "Ibis-Verlag",
            "firstPublished": "1947",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "lisleada-erzaehlg.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:02Z",
            "title": "Die Königin Ysabeau",
            "titleFileAs": "Königin Ysabeau, Die",
            "author": "Villiers de L'Isle-Adam",
            "pubId": "urn:uuid:8aa64ee8-87c7-4854-9f32-1baf4fb841c3",
            "year": [],
            "publisher": "Philipp Reclam jun. Leipzig",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "lisleada-2gesicht.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:01Z",
            "title": "Das zweite Gesicht und andere Novellen",
            "titleFileAs": "zweite Gesicht und andere Novellen, Das",
            "author": "Auguste de Villiers de L'Isle-Adam",
            "pubId": "urn:uuid:b1bf05d9-b446-47b6-a440-bf4e2d73db54",
            "year": [
                "1909"
            ],
            "publisher": "Buchverlag fürs Deutsche Haus",
            "firstPublished": "1909",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "lisleada-weissele.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:08:00Z",
            "title": "Legende vom weißen Elefanten",
            "titleFileAs": "Legende vom weißen Elefanten",
            "author": "Auguste de Villiers de L'Isle-Adam",
            "pubId": "urn:uuid:677a6624-8675-4c48-9543-2a50d5889f02",
            "year": [
                "1924"
            ],
            "publisher": "Georg Müller Verlag A.-G.",
            "firstPublished": "1924",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "lisleada-grausam.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:59Z",
            "title": "Grausame Geschichten",
            "titleFileAs": "Grausame Geschichten",
            "author": "Graf de Villiers de l'Isle Adam",
            "pubId": "urn:uuid:c6142ebf-99ae-4286-828d-dbb05da05a18",
            "year": [
                "1904"
            ],
            "publisher": "Verlag von E. Eißelt",
            "firstPublished": "1904",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "lisleada-axel.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:57Z",
            "title": "Villiers de l'Isle-Adam",
            "titleFileAs": "Villiers de l'Isle-Adam",
            "author": "Axel",
            "pubId": "urn:uuid:8c920083-3db1-49cb-ba19-6ff6ee82120b",
            "year": [
                "1920"
            ],
            "publisher": "Thespis-Verlag",
            "firstPublished": "1920",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "hoelderl-briefe.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:56Z",
            "title": "Brief an Susette Gontard",
            "titleFileAs": "Brief an Susette Gontard",
            "author": "Friedrich Hoelderlin",
            "pubId": "urn:isbn:3-458-14105-7",
            "year": [
                "1969"
            ],
            "publisher": "Beissner/Schmidt, Insel Verlag, Frankfurt",
            "firstPublished": "-",
            "sender": "HBrunner@aip.de"
        },
        {
            "htmlFileName": "hoelderl-saemtged.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:55Z",
            "title": "Gedichte 1784 - 1800",
            "titleFileAs": "Gedichte 1784 - 1800",
            "author": "Friedrich Hölderlin",
            "pubId": "urn:uuid:d4e35eca-e7f1-489a-b245-6a8beacd887c",
            "year": [
                "1946"
            ],
            "publisher": "J.G. Cottasche Buchhandlung Nachfolger",
            "firstPublished": "1946",
            "sender": "waltraud.lukasser@chello.at"
        },
        {
            "htmlFileName": "hoelderl-empedok.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:54Z",
            "title": "Empedokles",
            "titleFileAs": "Empedokles",
            "author": "Friedrich Hölderlin",
            "pubId": "urn:uuid:3f93bacd-b4f2-4712-9168-5047236d0f40",
            "year": [
                "1936"
            ],
            "publisher": "Hyperion-Verlag",
            "firstPublished": "1846",
            "sender": "jost.brachert.gmx.de"
        },
        {
            "htmlFileName": "hoelderl-hyperion.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:53Z",
            "title": "Hyperion",
            "titleFileAs": "Hyperion",
            "author": "Friedrich Hölderlin",
            "pubId": "urn:isbn:3-458-32065-2",
            "year": [
                "1979"
            ],
            "publisher": "Insel Verlag",
            "firstPublished": "1797",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "hoelderl-gedichte.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:52Z",
            "title": "Gedichte",
            "titleFileAs": "Gedichte",
            "author": "Friedrich Hölderlin",
            "pubId": "urn:uuid:be3a64d9-3da9-413b-bc6c-eb35ab05ff85",
            "year": [],
            "publisher": "Herausgeber unbekannt",
            "firstPublished": "-",
            "sender": "-"
        },
        {
            "htmlFileName": "turgenev-aufzeichn.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:50Z",
            "title": "Aufzeichnungen eines Jägers",
            "titleFileAs": "Aufzeichnungen eines Jägers",
            "author": "Iwan Turgenjew",
            "pubId": "urn:uuid:3693d375-dbc1-4c4b-9528-e0b4937a3771",
            "year": [
                "1960"
            ],
            "publisher": "Goldmanns Taschenbücher",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "turgenev-klaramil.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:49Z",
            "title": "Klara Militsch",
            "titleFileAs": "Klara Militsch",
            "author": "Iwan Turgenjew",
            "pubId": "urn:uuid:31857386-220d-4416-8e3a-ff02353eac8d",
            "year": [],
            "publisher": "Herausgeber unbekannt",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "turgenev-mumu.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:48Z",
            "title": "Mumu",
            "titleFileAs": "Mumu",
            "author": "Ivan Sergejevich Turgenev",
            "pubId": "urn:uuid:7babc280-058d-4547-8610-acb9ea16d73f",
            "year": [
                "1924"
            ],
            "publisher": "Verlag Erich Matthes",
            "firstPublished": "1924",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "turgenev-jugendze.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:47Z",
            "title": "Aus der Jugendzeit",
            "titleFileAs": "Aus der Jugendzeit",
            "author": "Iwan Turgénjew",
            "pubId": "urn:uuid:4d7580e0-ee72-4ab1-af6d-0155e30039bd",
            "year": [],
            "publisher": "Verlag von Otto Janke",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "turgenev-faust.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:46Z",
            "title": "Faust: Erzählung in neun Briefen",
            "titleFileAs": "Faust: Erzählung in neun Briefen",
            "author": "Ivan Sergejevich Turgenev",
            "pubId": "urn:uuid:24b709a9-8654-479b-9130-ce61ef067f15",
            "year": [
                "1990"
            ],
            "publisher": "Manesse Verlag",
            "firstPublished": "1862",
            "sender": "reuters@abc.de"
        },
        {
            "htmlFileName": "turgenev-visionen.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:45Z",
            "title": "Visionen und andere phantastische Erzählungen",
            "titleFileAs": "Visionen und andere phantastische Erzählungen",
            "author": "Iwan Turgenjew",
            "pubId": "urn:uuid:9c8236cf-a5e7-463a-8a44-048c335a9f44",
            "year": [
                "1917"
            ],
            "publisher": "Gustav Kiepenheuer Verlag",
            "firstPublished": "1917",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "turgenev-duellant.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:44Z",
            "title": "Der Duellant",
            "titleFileAs": "Duellant, Der",
            "author": "Iwan Turgenjew",
            "pubId": "urn:uuid:716e8273-637f-4e3c-87fb-6d0077fd4857",
            "year": [],
            "publisher": "Herausgeber unbekannt",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "jungstil-stjugend.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:43Z",
            "title": "Henrich Stillings Jugend / 1",
            "titleFileAs": "Henrich Stillings Jugend / 1",
            "author": "Johann Heinrich Jung-Stilling",
            "pubId": "urn:isbn:3-15-000662-7",
            "year": [
                "1997"
            ],
            "publisher": "Philipp Reclam jun.",
            "firstPublished": "1777",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "jungstil-misc.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:42Z",
            "title": "Eickels Verklärung",
            "titleFileAs": "Eickels Verklärung",
            "author": "Johann Heinrich Jung",
            "pubId": "urn:uuid:cac795d0-eed6-4e9f-a254-f2f79c8cadaa",
            "year": [],
            "publisher": "Herausgeber unbekannt",
            "firstPublished": "-",
            "sender": "Dr.Albrecht.Blank@t-online.de"
        },
        {
            "htmlFileName": "jungstil-stjuengl.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:40Z",
            "title": "Henrich Stillings Jünglings-Jahre / 1",
            "titleFileAs": "Henrich Stillings Jünglings-Jahre / 1",
            "author": "Johann Heinrich Jung-Stilling",
            "pubId": "urn:isbn:3-15-000662-7",
            "year": [
                "1997"
            ],
            "publisher": "Philipp Reclam jun.",
            "firstPublished": "1778",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "jungstil-stleben.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:39Z",
            "title": "Henrich Stillings häusliches Leben / 1",
            "titleFileAs": "Henrich Stillings häusliches Leben / 1",
            "author": "Johann Heinrich Jung-Stilling",
            "pubId": "urn:isbn:3-15-000662-7",
            "year": [
                "1997"
            ],
            "publisher": "Philipp Reclam jun.",
            "firstPublished": "1789",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "jungstil-stwander.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:38Z",
            "title": "Henrich Stillings Wanderschaft / 1",
            "titleFileAs": "Henrich Stillings Wanderschaft / 1",
            "author": "Johann Heinrich Jung-Stilling",
            "pubId": "urn:isbn:3-15-000662-7",
            "year": [
                "1997"
            ],
            "publisher": "Philipp Reclam jun.",
            "firstPublished": "1778",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "lachmanv-henna.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:37Z",
            "title": "Die 8 Henna-Legenden",
            "titleFileAs": "8 Henna-Legenden, Die",
            "author": "Volkmar Lachmann",
            "pubId": "urn:uuid:0ee2bd5d-6016-454f-89ac-15338af575d9",
            "year": [
                "1955"
            ],
            "publisher": "Scherpe-Verlag",
            "firstPublished": "-",
            "sender": "gerd.bouillon@t-online.de"
        },
        {
            "htmlFileName": "eckstein-arno.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:36Z",
            "title": "Fürst Arno",
            "titleFileAs": "Fürst Arno",
            "author": "Ernst Eckstein",
            "pubId": "urn:uuid:ee7fa250-7f4e-4df3-a0fd-2cbc4461ba59",
            "year": [],
            "publisher": "Max Hesses Verlag",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "eckstein-preisgek.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:35Z",
            "title": "Preisgekrönt",
            "titleFileAs": "Preisgekrönt",
            "author": "Ernst Eckstein",
            "pubId": "urn:uuid:f1b3b77c-dc65-443c-bf3e-5338e0d7e740",
            "year": [],
            "publisher": "Max Hesses Verlag",
            "firstPublished": "-",
            "sender": "www.gaga.net"
        },
        {
            "htmlFileName": "eckstein-schulhum.xhtml",
            "navFileName": "nav.xhtml",
            "tocFileName": "toc.ncx",
            "now": "2022-07-08T16:07:34Z",
            "title": "Gesammelte Schulhumoresken",
            "titleFileAs": "Gesammelte Schulhumoresken",
            "author": "Ernst Eckstein",
            "pubId": "urn:uuid:f3f8905f-53a5-4b1a-bab6-89d95ceae037",
            "year": [
                "ca. 1910"
            ],
            "publisher": "Verlag von J. Neumann",
            "firstPublished": "1907",
            "sender": "gerd.bouillon@t-online.de"
        }
    ]

    const log = (e: SyntheticEvent): void => {
        let inputElement = e.target as HTMLInputElement
        let text = inputElement.value.toLowerCase()
        let ntext = text.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
        console.log(ntext);
    }

    return (
        <div className="App">
            <header className="App-header">
                <h1>Projekt Gutenberg</h1>
                <input className="inputtextfind" type="text" onKeyUp={log}/>
                <table>
                    <tbody>
                    <tr>
                        <th>Author</th>
                        <th>Titel</th>
                        <th>Herausgeber</th>
                    </tr>
                    {data.map((val, key) => {
                        return (
                            <tr key={key}>
                                <td className="firstcolumn">{val.author}</td>
                                <td>{val.title}</td>
                                <td>{val.publisher}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </header>
        </div>
    );
}

export default App;
