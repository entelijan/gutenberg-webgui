export {}

const d1 = [
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "schul", "expected": true},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "ernst", "expected": true},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "schuld", "expected": false},
]

d1.forEach((obj) => {
  test(`String contains substring ${obj.a} ${obj.b}`, () => {
    let a = obj.a
    let b = obj.b
    expect(containsString(a, b)).toBe(obj.expected)
  })
})

const d2 = [
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "schul", "expected": true},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "ernst", "expected": true},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "ernst schul", "expected": true},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "schuld", "expected": false},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "schuld schul", "expected": false},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "ernst schuld schul", "expected": false},
  {"a": "ernstecksteingesammelteschulhumoreskenverlagvonjneumann", "b": "ernst sammel schul", "expected": true},
]

d2.forEach((obj) => {
  test(`String contains all substrings ${obj.a} ${obj.b}`, () => {
    let a = obj.a
    let b = obj.b
    expect(containsAllString(a, b)).toBe(obj.expected)
  })
})

function containsString(s: string, sub: string): Boolean {
  return s.includes(sub)
}

function containsAllString(s: string, input: string): Boolean {
  let result = true
  let words: string[] = input.split(/(\s+)/).filter( function(e) { return e.trim().length > 0; } )
  for (let word of words) {
    if (!containsString(s, word)) {
      result = false
      break
    }
  }
  return result
}